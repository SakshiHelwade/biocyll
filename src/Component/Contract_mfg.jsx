// import React from 'react'
// import "../Css/style.css"
// const Contract_mfg = () => {
//   return (
//     <>
//       <section className='journey-section'>
//         <div class="container-fluid">
//           <div class="row">
//             <div class="col-md-12">
//               <div class="journey-text">
//                 <h3 className='mb-2'>Complete Solution for Sustainable Farming Practices</h3>
//                 <p>Our contract manufacturing facility has been meticulously designed to adhere to global standards, boasting state-of-the-art technology for efficient and top-tier production. With robust systems and stringent procedures in place, we uphold stringent quality control measures at every phase of the production process, ensuring excellence in every product we deliver.

//                   At Biocyll Laboratories Pvt. Ltd., our dedication to sustainable agriculture drives our belief in the transformative potential of microbial solutions. By partnering with us, you gain access to our extensive product range, technical expertise, and diverse business opportunities. Our team comprises technical experts who collaborate closely to facilitate knowledge transfer and ensure the availability of our environmentally friendly solutions. With Biocyll Laboratories Pvt. Ltd., you not only receive high-quality products but also benefit from our unwavering commitment to sustainable farming practices and our comprehensive support network.

//                   To discover opportunities for mutually advantageous business partnerships. Reach out to us now!

//                 </p>
//               </div>
//             </div>
//           </div>
//         </div>
//       </section>
//     </>
//   )
// }

// export default Contract_mfg
import React from 'react';
// import "../Css/style.css";

const Contract_mfg = () => {
  return (
    <section className="journey-section">
      <div className="container-fluid">
        <div className="row justify-content-center">
          <div className="col-md-10">
            <div className="">
              <div className="header text-center">
                <h3 className="title text-success">
                  Complete Solution for Sustainable Farming Practices
                </h3>
              </div>
              <div className="body">
                <p className="text-justify mt-3">
                  In light of the unprecedented challenges facing our planet, the adoption of sustainable farming practices has become increasingly imperative. Biocyll Laboratories Pvt. Ltd. is at the forefront of this pivotal endeavor, offering a range of state-of-the-art microbial-based solutions that are not only eco-friendly but also highly effective. 
                </p>
                <p className="text-justify">
                  Our commitment to sustainability is unwavering, as evidenced by our products which promote environmentally responsible agriculture without compromising on quality. By collaborating closely with farmers and agricultural stakeholders, we tailor our microbial solutions to address their specific needs, ensuring optimal results. Our emphasis on innovation, technology, and sustainability affords us a distinct competitive edge, earning us the trust and loyalty of farmers as the foremost brand in the realm of biologicals.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contract_mfg;
