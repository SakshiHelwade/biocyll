import React from 'react'
// import "../Css/style.css"
import AOS from 'aos'
import { useEffect } from 'react';

const Quality_comp = () => {
  useEffect(() => {
    AOS.init({
      duration: 1000, // Animation duration
    })
  }, [])

  return (
    <>
      <section className='journey'>
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="journey-text">
                <h1 className='mb-2 text-success text-center'>Quality Compliance</h1><br />
                <p>
                  <h4 className='text-start'>Quality Assurance</h4><br />
                  <p className='' data-aos="fade-up">Our rigorous quality control procedures ensure that our microbial products align with client specifications, FCO and CIB regulations. We ensure compliance through stringent protocols governing the handling and storage of microorganisms, continuous monitoring for contamination, adherence to various standards, and meticulous documentation practices.</p><br />
                  <h4 className='text-start'>Adherence to Quality Standards</h4><br />
                  <p className='' data-aos="fade-up">Given that biological products consist of living microorganisms, they demand highly specialized protocols for handling, storage, and application. Our top-notch Quality Policy guarantees the superior quality of our products. Our production procedures strictly adhere to various standards, incorporating clean room facilities, among other measures. We meticulously implement quality procedures to achieve a contamination-free product.</p>

                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Quality_comp
