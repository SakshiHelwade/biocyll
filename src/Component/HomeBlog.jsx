// import React from "react";
// import OwlCarousel from "react-owl-carousel";
// import "owl.carousel/dist/assets/owl.carousel.min.css";
// import "owl.carousel/dist/assets/owl.theme.default.min.css";
// import "../Css/style.css"

// const Testimonial = () => {
//   const options = {
//     loop: true,
//     margin: 10,
//     nav: true,
//     responsive: {
//       0: {
//         items: 1,
//       },
//       600: {
//         items: 2,
//       },
//       1000: {
//         items: 3,
//       },
//     },
//   };

//   return (
//     <section class="gradient-custom">
//   <div class="container my-5 py-5">
//     <div class="row d-flex justify-content-center">
//       <div class="col-md-12">
//         <div class="text-center mb-4 pb-2">
//           <i class="fas fa-quote-left fa-3x text-white"></i>
//         </div>

//         <div class="card">
//           <div class="card-body px-4 py-5">
//             <div id="carouselDarkVariant" data-mdb-carousel-init class="carousel slide carousel-dark" data-mdb-ride="carousel">
//               <div class="carousel-indicators mb-0">
//                 <button data-mdb-button-init data-mdb-target="#carouselDarkVariant" data-mdb-slide-to="0" class="active"
//                   aria-current="true" aria-label="Slide 1"></button>
//                 <button data-mdb-button-init data-mdb-target="#carouselDarkVariant" data-mdb-slide-to="1"
//                   aria-label="Slide 1"></button>
//                 <button data-mdb-button-init data-mdb-target="#carouselDarkVariant" data-mdb-slide-to="2"
//                   aria-label="Slide 1"></button>
//               </div>

//               <div class="carousel-inner pb-5">
//                 <div class="carousel-item active">
//                   <div class="row d-flex justify-content-center">
//                     <div class="col-lg-10 col-xl-8">
//                       <div class="row">
//                         <div class="col-lg-4 d-flex justify-content-center">
//                           <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(1).webp"
//                             class="rounded-circle shadow-1 mb-4 mb-lg-0" alt="woman avatar" width="150"
//                             height="150" />
//                         </div>
//                         <div
//                           class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
//                           <h4 class="mb-4">Maria Smantha - Web Developer</h4>
//                           <p class="mb-0 pb-3">
//                             Lorem ipsum dolor sit amet, consectetur adipisicing elit. A
//                             aliquam amet animi blanditiis consequatur debitis dicta
//                             distinctio, enim error eum iste libero modi nam natus
//                             perferendis possimus quasi sint sit tempora voluptatem. Est,
//                             exercitationem id ipsa ipsum laboriosam perferendis.
//                           </p>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//                 <div class="carousel-item">
//                   <div class="row d-flex justify-content-center">
//                     <div class="col-lg-10 col-xl-8">
//                       <div class="row">
//                         <div class="col-lg-4 d-flex justify-content-center">
//                           <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(2).webp"
//                             class="rounded-circle shadow-1 mb-4 mb-lg-0" alt="woman avatar" width="150"
//                             height="150" />
//                         </div>
//                         <div
//                           class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
//                           <h4 class="mb-4">Lisa Cudrow - Graphic Designer</h4>
//                           <p class="mb-0 pb-3">
//                             Sed ut perspiciatis unde omnis iste natus error sit voluptatem
//                             accusantium doloremque laudantium, totam rem aperiam, eaque
//                             ipsa quae ab illo inventore veritatis et quasi architecto
//                             beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
//                             quia voluptas sit aspernatur.
//                           </p>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>

//                 <div class="carousel-item">
//                   <div class="row d-flex justify-content-center">
//                     <div class="col-lg-10 col-xl-8">
//                       <div class="row">
//                         <div class="col-lg-4 d-flex justify-content-center">
//                           <img src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(9).webp"
//                             class="rounded-circle shadow-1 mb-4 mb-lg-0" alt="woman avatar" width="150"
//                             height="150" />
//                         </div>
//                         <div
//                           class="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
//                           <h4 class="mb-4">John Smith - Marketing Specialist</h4>
//                           <p class="mb-0 pb-3">
//                             At vero eos et accusamus et iusto odio dignissimos qui
//                             blanditiis praesentium voluptatum deleniti atque corrupti quos
//                             dolores et quas molestias excepturi sint occaecati cupiditate
//                             non provident, similique sunt in culpa qui officia mollitia
//                             animi id laborum et dolorum fuga.
//                           </p>
//                         </div>
//                       </div>
//                     </div>
//                   </div>
//                 </div>
//               </div>

//               <button data-mdb-button-init class="carousel-control-prev" type="button" data-mdb-target="#carouselDarkVariant"
//                 data-mdb-slide="prev">
//                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
//                 <span class="visually-hidden">Previous</span>
//               </button>
//               <button data-mdb-button-init class="carousel-control-next" type="button" data-mdb-target="#carouselDarkVariant"
//                 data-mdb-slide="next">
//                 <span class="carousel-control-next-icon" aria-hidden="true"></span>
//                 <span class="visually-hidden">Next</span>
//               </button>
//             </div>
//           </div>
//         </div>

//         <div class="text-center mt-4 pt-2">
//           <i class="fas fa-quote-right fa-3x text-white"></i>
//         </div>
//       </div>
//     </div>
//   </div>
// </section>
//   );
// };

// export default Testimonial;
import React from "react";
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.min.css";
import "owl.carousel/dist/assets/owl.theme.default.min.css";
import "../Css/style.css";

const HomeBlog = () => {
  const options = {
    loop: true,
    margin: 10,
    nav: true,
    items: 1,  // Show only one item at a time
    responsive: {
      0: {
        items: 1, // 1 item on small screens
      },
      600: {
        items: 1, // 1 item on medium screens
      },
      1000: {
        items: 1, // 1 item on large screens
      },
    },
  };

  return (
    <section className="gradient-custom">
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-md-12">
          <div className="text-center">
              <h2 className="text-black">Blogs</h2>
            </div>
            <div className="text-center">
              <i className="fas fa-quote-left fa-3x text-white"></i>
            </div>

            <div className="card">
            <div className="card-body px-4 py-5">
                <OwlCarousel className="owl-theme" {...options}>
                  <div className="item">
                    <div className="row d-flex justify-content-center">
                      <div className="col-lg-10 col-xl-8">
                        <div className="row">
                          {/* <div className="col-lg-4 d-flex justify-content-center">
                            <img
                              src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(1).webp"
                              className="rounded-circle shadow-1 mb-4 mb-lg-0"
                              alt="woman avatar"
                              width="150"
                              height="150"
                            />
                          </div> */}
                          <div className="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                            <h4 className="mb-4">Maria Smantha - Web Developer</h4>
                            <p className="mb-0 pb-3">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi
                              blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi
                              nam natus perferendis possimus quasi sint sit tempora voluptatem.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="item">
                    <div className="row d-flex justify-content-center">
                      <div className="col-lg-10 col-xl-8">
                        <div className="row">
                          <div className="col-lg-4 d-flex justify-content-center">
                            {/* <img
                              src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(2).webp"
                              className="rounded-circle shadow-1 mb-4 mb-lg-0"
                              alt="woman avatar"
                              width="150"
                              height="150"
                            /> */}
                          </div>
                          <div className="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                            <h4 className="mb-4">Lisa Cudrow - Graphic Designer</h4>
                            <p className="mb-0 pb-3">
                              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                              doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                              veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* New Testimonial */}
                  <div className="item">
                    <div className="row d-flex justify-content-center">
                      <div className="col-lg-10 col-xl-8">
                        <div className="row">
                          <div className="col-lg-4 d-flex justify-content-center">
                            {/* <img
                              src="https://mdbcdn.b-cdn.net/img/Photos/Avatars/img%20(3).webp"
                              className="rounded-circle shadow-1 mb-4 mb-lg-0"
                              alt="man avatar"
                              width="150"
                              height="150"
                            /> */}
                          </div>
                          <div className="col-9 col-md-9 col-lg-7 col-xl-8 text-center text-lg-start mx-auto mx-lg-0">
                            <h4 className="mb-4">Alex Johnson - UI/UX Designer</h4>
                            <p className="mb-0 pb-3">
                              Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aliquam amet animi
                              blanditiis consequatur debitis dicta distinctio, enim error eum iste libero modi
                              nam natus perferendis possimus quasi sint sit tempora voluptatem.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </OwlCarousel>
              </div>
            </div>

            <div className="text-center">
              <i className="fas fa-quote-right fa-3x text-white"></i>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default HomeBlog;
