
// import React from 'react';

// const ProductPortfolio = () => {
//   const styles = {
//     container: {
      
//       padding: '20px',
//       maxWidth: '800px',
//       margin: '0 auto',
//       backgroundColor: '#f9f9f9', 
//       borderRadius: '8px',
//       boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
//     },
//     header: {
//       textAlign: 'center',
//       marginBottom: '20px',
//       color: '#333',
//     },
//     sectionTitle: {
//       color: '#2c3e50',
//       margin: '20px 0 10px 0',
//       fontSize: '18px',
//       fontWeight: 'bold',
//     },
//     paragraph: {
//       color: '#555',
//       lineHeight: '1.6',
//       marginBottom: '15px',
//       fontSize: '19px',
//     },
//     highlight: {
//       backgroundColor: '#eaf2f8', 
//       padding: '10px',
//       borderRadius: '5px',
//       borderLeft: '5px solid #2c3e50', 
//     },
//   };

//   return (
//     <div style={styles.container}>
//       <h1 style={styles.header}>Product Portfolio</h1>
//       <h4 style={styles.sectionTitle}>
//         <b>Evaluation and Validation</b>
//       </h4>
//       <p style={styles.paragraph}>
//         Our groundbreaking microbial agricultural solutions have undergone rigorous testing and implementation worldwide, spanning various agro-climatic regions and soil compositions across a wide array of crops. They have consistently surpassed both traditional chemical inputs and rival biological products in performance.
//       </p>
//       <h4 style={styles.sectionTitle}>
//         <b>Tailoring Solutions</b>
//       </h4>
//       <p style={styles.paragraph}>
//         Recognizing the diverse needs of various markets, Biocyll Laboratories Pvt. Ltd. possesses the capability to craft personalized microbial solutions tailored to specific market requirements. To facilitate farmer convenience, our formulations are accessible in Liquid, Powder (Dextrose, Calcite and Customized), Tablets, Capsules and Granules for effortless application.
//       </p>
//       <h4 style={styles.sectionTitle}>
//         <b>Advantages of Microbial Inputs</b>
//       </h4>
//       <p style={{ ...styles.paragraph, ...styles.highlight }}>
//         Environmentally Responsible Solutions <br />
//         Adhering to Regulations <br />
//         Promoting Sustainable Farming
//       </p>
//       <h4 style={styles.sectionTitle}>
//         <b>Technical Assistance and Commercial Prospects</b>
//       </h4>
//       <p style={styles.paragraph}>
//         Our expertise lies in providing technical guidance, regulatory affairs support, timely on-site assistance, and comprehensive techno-commercial training on biological inputs and farming practices to the field personnel of potential and current business partners. Additionally, we extend various business prospects including co-marketing initiatives, private label collaborations, and contract manufacturing arrangements.
//       </p>
//       <h4 style={styles.sectionTitle}>
//         <b>High-Quality Contract Manufacturing and Expert Team</b>
//       </h4>
//       <p style={styles.paragraph}>
//         Our contract manufacturing facility has been meticulously designed to adhere to global standards, boasting state-of-the-art technology for efficient and top-tier production. With robust systems and stringent procedures in place, we uphold stringent quality control measures at every phase of the production process, ensuring excellence in every product we deliver.
//       </p>
//       <p style={styles.paragraph}>
//         At Biocyll Laboratories Pvt. Ltd., our dedication to sustainable agriculture drives our belief in the transformative potential of microbial solutions. By partnering with us, you gain access to our extensive product range, technical expertise, and diverse business opportunities. Our team comprises technical experts who collaborate closely to facilitate knowledge transfer and ensure the availability of our environmentally friendly solutions. With Biocyll Laboratories Pvt. Ltd., you not only receive high-quality products but also benefit from our unwavering commitment to sustainable farming practices and our comprehensive support network.
//       </p>
//       <h4 style={styles.sectionTitle}>
//         <b>To discover opportunities for mutually advantageous business partnerships. Reach out to us now!</b>
//       </h4>
//     </div>
//   );
// };

// export default ProductPortfolio;
import React from 'react';

const ProductPortfolio = () => {
  return (
    <div style={{
      padding: '20px',
      maxWidth: '1200px',
      margin: '0 auto',
      backgroundColor: '#f9f9f9',
      borderRadius: '8px',
      boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)'
    }}>
      <h1 style={{
        textAlign: 'center',
        marginBottom: '20px',
        color: '#333'
      }}>Product Portfolio</h1><br/>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        textAlign:'center'
        // fontWeight: 'bold'
      }}>
        <b>Evaluation and Validation</b>
      </h4>
      <p style={{
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        Our groundbreaking microbial agricultural solutions have undergone rigorous testing and implementation worldwide, spanning various agro-climatic regions and soil compositions across a wide array of crops. They have consistently surpassed both traditional chemical inputs and rival biological products in performance.
      </p>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        // fontWeight: 'bold'
        textAlign:'center'

      }}>
        <b>Tailoring Solutions</b>
      </h4>
      <p style={{
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        Recognizing the diverse needs of various markets, Biocyll Laboratories Pvt. Ltd. possesses the capability to craft personalized microbial solutions tailored to specific market requirements. To facilitate farmer convenience, our formulations are accessible in Liquid, Powder (Dextrose, Calcite and Customized), Tablets, Capsules and Granules for effortless application.
      </p>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        // fontWeight: 'bold'
        textAlign:'center'
      }}>
        <b>Advantages of Microbial Inputs</b>
      </h4>
      <p style={{
        backgroundColor: '#eaf2f8',
        padding: '10px',
        borderRadius: '5px',
        borderLeft: '5px solid #2c3e50',
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        Environmentally Responsible Solutions <br />
        Adhering to Regulations <br />
        Promoting Sustainable Farming
      </p>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        textAlign:'center'
        // fontWeight: 'bold'
      }}>
        <b>Technical Assistance and Commercial Prospects</b>
      </h4>
      <p style={{
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        Our expertise lies in providing technical guidance, regulatory affairs support, timely on-site assistance, and comprehensive techno-commercial training on biological inputs and farming practices to the field personnel of potential and current business partners. Additionally, we extend various business prospects including co-marketing initiatives, private label collaborations, and contract manufacturing arrangements.
      </p>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        textAlign:'center'
        // fontWeight: 'bold'
      }}>
        <b>High-Quality Contract Manufacturing and Expert Team</b>
      </h4>
      <p style={{
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        Our contract manufacturing facility has been meticulously designed to adhere to global standards, boasting state-of-the-art technology for efficient and top-tier production. With robust systems and stringent procedures in place, we uphold stringent quality control measures at every phase of the production process, ensuring excellence in every product we deliver.
      </p>

      <p style={{
        color: '#555',
        lineHeight: '1.6',
        marginBottom: '15px',
        fontSize: '20px'
      }}>
        At Biocyll Laboratories Pvt. Ltd., our dedication to sustainable agriculture drives our belief in the transformative potential of microbial solutions. By partnering with us, you gain access to our extensive product range, technical expertise, and diverse business opportunities. Our team comprises technical experts who collaborate closely to facilitate knowledge transfer and ensure the availability of our environmentally friendly solutions. With Biocyll Laboratories Pvt. Ltd., you not only receive high-quality products but also benefit from our unwavering commitment to sustainable farming practices and our comprehensive support network.
      </p>

      <h4 style={{
        color: '#2c3e50',
        margin: '20px 0 10px',
        fontSize: '18px',
        // fontWeight: 'bold'
        textAlign:'center'
      }}>
        <b>To discover opportunities for mutually advantageous business partnerships. Reach out to us now!</b>
      </h4>
    </div>
  );
};

export default ProductPortfolio;
