// import React from 'react'
// import "../Css/style.css"
// import Farmer from '../Assets/Images/farmer_journey.png'
// const JourneyHome = () => {
//   return (
//     <>
//       <section style={{fontSize:'115%'}}>
//         <div className='row'>
//           <div className='col-md-12 text-center'>
//             <h2>Our Journey</h2>
//           </div>
//         </div>

//         <div className='container-fluid' style={{backgroundColor:'#90d576',  boxShadow: '0 -4px 20px 0 #90d576'}}>
//           <div className="row" style={{ textAlign: 'justify' }}>
//             <div className="col-md-6 mt-4 journey-text p-5" >
//               <h4 style={{ lineHeight: 1.5 }}>Started back in 2012 in a very tiny manufacturing facility of 500 sq.ft. area, Biocyll draws inspiration from the noble thought of helping farmers with innovative solutions leading to more yield, helping them achieve prosperity in farming. Starting with the combined experience of more than 50 years of promoters, Biocyll has come so far as to acquire many technologies and products from industry, institutions, and industry stalwarts. Now Biocyll is impacting more than millions of farmers and is recognized as the Best Company in Biologicals by Agriculture Today, Biocyll Laboratories Pvt. Ltd. stands as a testament to the growth and pioneering advancements in biological technologies.</h4>
//             </div>
//             <div className="col-md-6" >
//               <img src={Farmer} className='w-100 mt-4'   />
//             </div>
//           </div>
//         </div>
//       </section>
//     </>
//   )
// }

// export default JourneyHome
import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css"; // Ensure Bootstrap is imported
import "aos/dist/aos.css"; // Ensure AOS is imported
import Aos from "aos";

const ProductSection = () => {
  useEffect(() => {
    Aos.init(); // Initialize AOS
  }, []);

  return (
    <section className="based-prod-sec py-5">
      <div className="container">
        <div className="row">
          {/* Neem Based Products Card */}
          <div
            className="col-lg-6 col-md-12 mb-4 aos-init aos-animate"
            data-aos="fade-up"
            data-aos-duration="1000" // Optional, can adjust timing of fade
            style={{
              backgroundImage: "url('https://kaybeebio.com/wp-content/uploads/2023/07/card.jpg')",
              backgroundSize: "cover",
              backgroundPosition: "center",
              height: "350px",
              borderRadius: "10px",
            }}
          >
            <div className="based-prod-content text-white d-flex flex-column justify-content-end p-4">
              {/* <h3>
                <strong>Neem</strong>
                <br />
                Based Products
              </h3>
              <ul>
                <li>Azadirachtin Technical Manufacturer</li>
                <li>6 Pack Study</li>
                <li>University Trials (SAU’s)</li>
              </ul> */}
              <a
                href=""
                className="btn btn-light mt-3"
              >
                View Products
              </a>
            </div>
            <figure className="text-center mt-3">
              <img
                src="https://kaybeebio.com/wp-content/uploads/2023/05/neem-img.png"
                className="img-fluid"
                alt="Neem Product"
                width="200"
                height="300"
                loading="lazy"
              />
            </figure>
          </div>

          {/* Granule Based Products Card */}
          <div
            className="col-lg-6 col-md-12 mb-4 aos-init aos-animate"
            data-aos="fade-up"
            data-aos-duration="1000" // Optional, can adjust timing of fade
            style={{
              backgroundImage: "url('https://kaybeebio.com/wp-content/uploads/2023/07/19-june-banner-image.jpg')",
              backgroundSize: "cover",
              backgroundPosition: "center",
              height: "350px",
              borderRadius: "10px",
            }}
          >
            <div className="based-prod-content text-white d-flex flex-column justify-content-end p-4">
              {/* <h3>
                <strong>Granule</strong>
                <br />
                Based Products
              </h3>
              <ul>
                <li>
                  Developed by a highly experienced team of experts that confirms your investment in a
                  premium and sustainable solution.
                </li>
              </ul> */}
              <a
                href=""
                className="btn btn-light mt-3"
              >
                View Products
              </a>
            </div>
            <figure className="text-center mt-3">
              <img
                src="https://kaybeebio.com/wp-content/uploads/2023/06/granule-product-home.png"
                className="img-fluid"
                alt="Granule Product"
                width="200"
                height="300"
                loading="lazy"
              />
            </figure>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ProductSection;

