

// import React from 'react'
// import { NavLink } from 'react-router-dom'
// import s3 from "../Assets/Images/s3.png"
// import s4 from "../Assets/Images/s4.png"
// import s5 from "../Assets/Images/s5.png"
// import s6 from "../Assets/Images/s6.png"
// import s7 from "../Assets/Images/s7.png"

// const Values = () => {
//   return (
//     <>
//         <section className='journey-section'>

//     <div className="row responsive-text" >
//       <div className="col-12 pr-5 pl-5 text-center  headings" >
//       <h3 className=" ">Core Values</h3>
//         <div className='journey-textt'>
//             <p className=' p-3' >Get to know us better with these core values that define us not only as a company but as a group of like-minded individuals who have a common goal of creating a better tomorrow.
//             </p>
//         </div>
//         </div>
//     </div>
//     <div className="responsive-text container-fluid pl-5 pb-5 lr-5 services " >
//                 <div className=" responsive-text row  p-3">
//                     <div className="col-md-8 mt-3 service_area " >
//                         <div className="row">
//                             <div className="col-4">
//                                 <img src={s3}  className='w-50'/>
//                             </div>
//                             <div className="col-8 headings  p-3 " >
//                                 <h5>ETHICS</h5>
//                                <p>We conduct ourselves and our business affairs in accordance with the highest ethical standards.</p>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-8 mt-3 service_area " style={{marginLeft:300}}>
//                         <div className="row">
//                             <div className="col-4">
//                                 <img src={s4}  className='w-50'/>
//                             </div>
//                             <div className="col-8 headings">
//                                 <h5>LONG TERM</h5>
//                                <p>We base our decisions on maximizing the long-term success of our company.</p>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-8 mt-3 service_area" > 
//                         <div className="row">
//                             <div className="col-4">
//                                 <img src={s4}  className='w-50'/>
//                             </div>
//                             <div className="col-8 headings">
//                                 <h5>CUSTOMER FIRST</h5>
//                                <p >We will work towards a customer focussed organization, keeping the customer front and center in all we do.</p>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-8 mt-3 service_area " style={{marginLeft:300}}>
//                         <div className="row">
//                             <div className="col-4">
//                                 <img src={s5}  className='w-50'/>
//                             </div>
//                             <div className="col-8 headings">
//                                 <h5>OWNERSHIP </h5>
//                                <p>Each individual doing what it takes to win.</p>
//                             </div>
//                         </div>
//                     </div>
//                     <div className="col-md-8 mt-3 service_area " >
//                         <div className="row">
//                             <div className="col-4">
//                                 <img src={s6}  className='w-50'/>
//                             </div>
//                             <div className="col-8 headings">
//                                 <h5> QUALITY</h5>
//                                <p>We take responsibility for quality. Our product and services will be amongst the best in terms of value delivered.</p>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </section>
//     </>
//   )
// }

// export default Values
import React, { useEffect } from 'react';
import AOS from 'aos';
import 'aos/dist/aos.css';
import s3 from "../Assets/Images/s3.png";
import s4 from "../Assets/Images/s4.png";
import s5 from "../Assets/Images/s5.png";
import s6 from "../Assets/Images/s6.png";

const Values = () => {
  useEffect(() => {
    AOS.init({ duration: 1000 }); // Initialize AOS with animation duration
  }, []);

  return (
    <>
      <section className='journey'>
        <div className="row responsive-text">
          <div className="col-12 pr-5 pl-5 text-center headings">
            <h1 className="text-success">Core Values</h1>
            <div className='journey-textt'>
              <p className='p-3' data-aos="fade-up">
                Get to know us better with these core values that define us not only as a company but as a group of like-minded individuals who have a common goal of creating a better tomorrow.
              </p>
            </div>
          </div>
        </div>
        <div className="responsive-text  pl-5 pb-5 lr-5 services">
          <div className="responsive-text row p-3">
            {/* ETHICS */}
            <div className="col-md-8 mt-3 service_area" data-aos="fade-right">
              <div className="row">
                <div className="col-4">
                  <img src={s3} className='w-50' alt="Ethics" />
                </div>
                <div className="col-8 headings p-3">
                  <h5>ETHICS</h5>
                  <p>We conduct ourselves and our business affairs in accordance with the highest ethical standards.</p>
                </div>
              </div>
            </div>
            {/* LONG TERM */}
            <div className="col-md-8 mt-3 service_area" style={{ marginLeft: 300 }} data-aos="fade-left">
              <div className="row">
                <div className="col-4">
                  <img src={s4} className='w-50' alt="Long Term" />
                </div>
                <div className="col-8 headings">
                  <h5>LONG TERM</h5>
                  <p>We base our decisions on maximizing the long-term success of our company.</p>
                </div>
              </div>
            </div>
            {/* CUSTOMER FIRST */}
            <div className="col-md-8 mt-3 service_area" data-aos="fade-right">
              <div className="row">
                <div className="col-4">
                  <img src={s4} className='w-50' alt="Customer First" />
                </div>
                <div className="col-8 headings">
                  <h5>CUSTOMER FIRST</h5>
                  <p>We will work towards a customer-focused organization, keeping the customer front and center in all we do.</p>
                </div>
              </div>
            </div>
            {/* OWNERSHIP */}
            <div className="col-md-8 mt-3 service_area" style={{ marginLeft: 300 }} data-aos="fade-left">
              <div className="row">
                <div className="col-4">
                  <img src={s5} className='w-50' alt="Ownership" />
                </div>
                <div className="col-8 headings">
                  <h5>OWNERSHIP</h5>
                  <p>Each individual doing what it takes to win.</p>
                </div>
              </div>
            </div>
            {/* QUALITY */}
            <div className="col-md-8 mt-3 service_area" data-aos="fade-right">
              <div className="row">
                <div className="col-4">
                  <img src={s6} className='w-50' alt="Quality" />
                </div>
                <div className="col-8 headings">
                  <h5>QUALITY</h5>
                  <p>We take responsibility for quality. Our product and services will be amongst the best in terms of value delivered.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default Values;
