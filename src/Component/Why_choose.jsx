import React from 'react'
import "../Css/style.css"

const Why_choose = () => {
  return (
    <>
      <section className="journey-section" style={{ fontSize: "115%" }}>
        <div className="container-fluid px-4 py-5">
          <div className="row justify-content-center">
            <div className="col-md-10">
              <div className="journey-text p-4 bg-light rounded shadow">
                <h3 className="mb-4 text-center text-success">Why Choose Us as Your Sustainable Solution Partner?</h3>
                <p>
                  Our B2B solutions deliver sustainable agriculture solutions to the agricultural sector. We specialize in tailoring microbial solutions to industrial needs, ensuring our clients receive the most optimal solution for their microbe-based product requirements. As awareness of the environmental impact of conventional agriculture rises, there is a growing inclination towards eco-friendly alternatives. Biocyll Laboratories Pvt. Ltd. is strategically positioned to capitalize on this shift, offering pioneering solutions to advance sustainable agriculture.
                </p>
                <p>
                  At Biocyll Laboratories Pvt. Ltd., we advocate for sustainable agriculture through the adoption of Integrated Nutrient Management (INM) and Integrated Pest Management (IPM) strategies. Our INM methodology ensures plants receive nutrients in a balanced manner, enhancing soil health and fertility. Conversely, our IPM approach focuses on pest management methods that minimize environmental harm and support sustainable farming.
                </p>
                <p>
                  Through our tailored microbial solutions, we facilitate sustainable agricultural practices. Collaborating with us enables Agri-input companies to access innovative solutions, fostering profitable agricultural practices. Simultaneously, farmers can enjoy higher yields and enhanced return on investment (ROI) by leveraging our offerings.
                </p>
                <h5 className="text-center mt-4 text-success"><b>Evaluation and Validation</b></h5>
                <p>
                  Our groundbreaking microbial agricultural solutions have undergone rigorous testing and implementation worldwide, spanning various agro-climatic regions and soil compositions across a wide array of crops. They have consistently surpassed both traditional chemical inputs and rival biological products in performance.
                </p>
                <h5 className="text-center mt-4 text-success"><b>Tailoring Solutions</b></h5>
                <p>
                  Recognizing the diverse needs of various markets, Biocyll Laboratories Pvt. Ltd. possesses the capability to craft personalized microbial solutions tailored to specific market requirements. To facilitate farmer convenience, our formulations are accessible in Liquid, Powder (Dextrose, Calcite and Customized), Tablets, Capsules, and Granules for effortless application.
                </p>
                <b className="d-block my-3">Advantages of Microbial Inputs:</b>
                <ul className="list-unstyled">
                  <li><i className="fa fa-check-circle text-success"></i> Environmentally Responsible Solutions</li>
                  <li><i className="fa fa-check-circle text-success"></i> Adhering to Regulations</li>
                  <li><i className="fa fa-check-circle text-success"></i> Promoting Sustainable Farming</li>
                </ul>
                <h5 className="text-center mt-4 text-success"><b>Technical Assistance and Commercial Prospects</b></h5>
                <p>
                  Our expertise lies in providing technical guidance, regulatory affairs support, timely on-site assistance, and comprehensive techno-commercial training on biological inputs and farming practices to the field personnel of potential and current business partners. Additionally, we extend various business prospects including co-marketing initiatives, private label collaborations, and contract manufacturing arrangements.
                </p>
                <h5 className="text-center mt-4 text-success"><b>High-Quality Contract Manufacturing and Expert Team</b></h5>
                <p>
                  Our contract manufacturing facility has been meticulously designed to adhere to global standards, boasting state-of-the-art technology for efficient and top-tier production. With robust systems and stringent procedures in place, we uphold stringent quality control measures at every phase of the production process, ensuring excellence in every product we deliver.
                </p>
                <p>
                  At Biocyll Laboratories Pvt. Ltd., our dedication to sustainable agriculture drives our belief in the transformative potential of microbial solutions. By partnering with us, you gain access to our extensive product range, technical expertise, and diverse business opportunities. Our team comprises technical experts who collaborate closely to facilitate knowledge transfer and ensure the availability of our environmentally friendly solutions. With Biocyll Laboratories Pvt. Ltd., you not only receive high-quality products but also benefit from our unwavering commitment to sustainable farming practices and our comprehensive support network.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>

    </>

  )
}

export default Why_choose
