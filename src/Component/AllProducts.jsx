
// import Slider from "react-slick";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import { NavLink, useParams } from "react-router-dom";
// import { UserContext } from "../Context/CreateContext";
// import { useContext, useEffect, useState } from "react";
// import { base_url } from "../Config/Index";
// import axios from "axios";
// import { ToastContainer, toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";

// export default function AllProducts({ product }) {
//   const { token } = useContext(UserContext);
//   const { id } = useParams();
//   const [allProducts, setAllProducts] = useState([]);
//   const [selectedCategory, setSelectedCategory] = useState("");
//   const [cartItems, setCartItems] = useState([]);
//   const [cartId, setcartId] = useState();
//   console.log(token, "ttt")
//   useEffect(() => {
//     getProduct();
//   }, [id]);

//   useEffect(() => {
//     getAllProduct();
//   }, []);

//   useEffect(() => {
//     getCartItems();
//   }, []);

//   const handleCart = async (productId) => {
//     const payload = {
//       product: productId,
//       quantity: 1,
//     };
//     try {
//       const response = await axios.post(`${base_url}/api/post/card`, payload, {
//         headers: {
//           Authorization: `${token}`,
//         },
//       });
//       toast.success("Item Added Successfully");
//       console.log(response.data, "add")
//       getCartItems();
//     } catch (error) {
//       console.log(error);
//     }
//   };

//   const getProduct = async () => {
//     try {
//       const response = await axios.get(`${base_url}/api/get/product/${id}`, {
//         headers: {
//           Authorization: `${token}`,
//         },
//       });
//       setSelectedCategory(response.data.category);
//     } catch (error) {
//       console.error("Error:", error.message);
//     }
//   };

//   const getAllProduct = async () => {
//     try {
//       const response = await axios.get(
//         `${base_url}/api/get/all/products?categoryId=${selectedCategory}`,
//         {
//           headers: {
//             Authorization: `${token}`,
//           },
//         }
//       );
//       const filteredProducts = response.data.filter((item) => item._id !== id);
//       setAllProducts(filteredProducts);
//     } catch (error) {
//       console.error("Error:", error.message);
//     }
//   };

//   const getCartItems = async () => {
//     try {
//       const response = await axios.get(`${base_url}/api/get/card`, {
//         headers: {
//           Authorization: `${token}`,
//         },
//       });
//       setCartItems(response.data.items);
//       setcartId(response.data._id);
//       console.log(response.data._id, "cartId")
//     } catch (error) {
//       console.log(error);
//     }
//   };


//   const handleQuantity = async (itemId, action) => {
//     try {
//       const response = await axios.put(
//         `${base_url}/api/carts/${cartId}/items/${itemId}`,
//         { action: action },
//         {
//           headers: {
//             Authorization: `${token}`,
//           },
//         }
//       );
//       console.log(response, "resp");
//       if (response.status === 200) {
//         getCartItems();
//       }
//     } catch (error) {
//       toast.error("Item Out of Stock");
//     }
//   };
//   const isInCart = (productId) => {
//     return cartItems.some((item) => item.product?._id === productId);
//   };

//   console.log(cartItems, "cartitems")

//   var settings = {
//     dots: false,
//     infinite: true,
//     speed: 500,
//     slidesToShow: 4,
//     slidesToScroll: 4,
//     autoplay: true,
//     autoplaySpeed: 3000,
//     responsive: [
//       {
//         breakpoint: 1024,
//         settings: {
//           slidesToShow: 3,
//           slidesToScroll: 3,
//         },
//       },
//       {
//         breakpoint: 600,
//         settings: {
//           slidesToShow: 2,
//           slidesToScroll: 1,
//         },
//       },
//       {
//         breakpoint: 480,
//         settings: {
//           slidesToShow: 1,
//           slidesToScroll: 1,
//         },
//       },
//     ],
//   };

//   return (
//     <>
//       <h2 className="text-center mt-3">
//         Products For You
//       </h2>
//       <section className="p-3 mb-5" style={{ backgroundColor: "#90d576", boxShadow: '0 -4px 20px 0 #90d576' }}>
//         <div className="outerDiv" style={{ backgroundColor: "#90d576" }}>
//           <Slider {...settings}>
//             {allProducts.map((item, index) => (
//               <div key={index}>
//                 <div
//                   className="card product_div"
//                   style={{
//                     backgroundColor: "white",
//                     borderRadius: "15px",
//                     padding: "10px",
//                     margin: "10px",
//                     height: "350px",
//                     width: "250px"
//                   }}
//                 >
//                   <NavLink to={`/product_details/${item?._id}`}>
//                     <div className="card-header image_div">
//                       <img
//                         src={item.images[0].url}
//                         className="w-100 h-300"
//                         alt={item.title}
//                         style={{ borderRadius: "10px" }}
//                       />
//                     </div>
//                   </NavLink>
//                   <div
//                     className="card-body details_div text-center"
//                     style={{ fontSize: 12, fontWeight: "bold", marginTop: "10px" }}
//                   >
//                     {item.title}
//                     <br />
//                     <span style={{ color: "green", fontSize: 12 }}>
//                       Rs.
//                       {item.price}
//                       <br />
//                     </span>
//                   </div>
//                   <div className="card-footer p-0">
//                     {isInCart(item._id) ? (
//                       <div className="d-flex justify-content-between align-items-center">
//                         <button
//                           onClick={() => handleQuantity(item._id, "decrement")}
//                           className="btn btn-sm border border-success"
//                         >
//                           -
//                         </button>
//                         <span className="mx-2" style={{ color: "black", fontSize: 15, marginBottom: "10px" }}>
//                           {cartItems.find((cartItem) => cartItem.product._id === item._id)?.quantity}
//                         </span>
//                         <button
//                           className="btn btn-sm border border-success"
//                           onClick={() => handleQuantity(item._id, "increment")}
//                         >
//                           +
//                         </button>
//                       </div>
//                     ) : (
//                       <button
//                         className="btn btn-sm btn-success w-500"
//                         onClick={() => handleCart(item._id)}
//                         style={{ borderRadius: "5px", marginTop: 20 }}
//                       >
//                         Add to Cart
//                       </button>
//                     )}

//                   </div>
//                 </div>
//               </div>
//             ))}
//           </Slider>
//         </div>
//         {/* <NavLink to="/product_page">
//           <button
//             className="btn text-primary"
//             style={{
//               backgroundColor: "transparent",
//               border: 0,
//               // float: "right",
//               position: "absolute", 
//       // top: "2px", 
//       right: "10px", 
//       bottom:"-500px"

//             }}
//           >
//             View More
//           </button>
//         </NavLink> */}
//       </section>
//       <ToastContainer />
//     </>
//   );
// }



import Img4 from "../Assets/Images/Img4.jpeg";
import React, { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';  // Ensure Bootstrap is imported
import 'aos/dist/aos.css';  // Ensure AOS is imported
import Aos from "aos";

const FarmerImageSection = () => {
  useEffect(() => {
    Aos.init();  // Initialize AOS
  }, []);
  useEffect(() => {
        Aos.init({
          duration: 2000,  // Set default duration to 2 seconds (2000ms)
        });
      }, []);

  return (
    <div className="container py-5">
     <div className="row align-items-center">
  {/* Card Section with Fade Right Effect for Paragraph */}
  <div className="col-lg-6 col-md-12">
    <h1 className="text-danger mb-4">
      BOTANICAL BASED BIO-PESTICIDES MANUFACTURER IN INDIA
    </h1>

    {/* Paragraph with Fade Right Effect */}
    <p
      className="text-secondary"
      data-aos="fade-right"
      data-aos-duration="2000"
      style={{ fontSize: "1.2rem" }} // Increase font size
    >
      Kay Bee Bio-Organics offers a wide range of ECOCERT-approved, NOP-NPOP
      certified, and patented pest management solutions which include
      Bio-insecticides, Bio-Fungicides, Bio-Acaricides, Adjuvants, and CIBRC
      certified{" "}
      <a
        href="https://kaybeebio.com/product-category/neem-based-products/"
        target="_blank"
        rel="noopener"
      >
        Neem Based Products
      </a>
      . And each product category has come out with solutions that are
      unparalleled. Kay Bee Bio Organics aims to become a force to reckon with
      within organic farming.
    </p>

    <p
      className="text-secondary"
      data-aos="fade-right"
      data-aos-duration="2000"
      style={{ fontSize: "1.2rem" }} // Increase font size
    >
      We want to reduce the cost of production for farmers as we try to sustain
      biodiversity and healthy living through our products. All of it while
      replacing harmful synthetic chemical pesticides with organic ones,
      globally. Every day our research and development teams work towards
      crafting botanical-based pesticides from plant alkaloids so that we can
      meet the export standards and at the same time, keep mother earth safe
      from the hazardous residue effect of chemicals.
    </p>

    <a href="about us" className="btn btn-danger"> 
      Know More
    </a>
  </div>

  {/* Farmer Image Section with Zoom-In Effect */}
  <div className="col-lg-6 col-md-12 text-center">
    <div
      className="hm-kb-org-thumb aos-init aos-animate"
      data-aos="zoom-in"
    >
      <a
        href="https://www.youtube.com/watch?v=LhruK1TUK00"
        data-fancybox="banner"
      >
        <figure>
          <img
            src={Img4} // Replace with your farmer image
            className="img-fluid"
            alt="Farmer"
            width="600"
            height="450"
            loading="lazy"
          />
        </figure>
      </a>
    </div>
  </div>
</div>

    </div>
  );
};

export default FarmerImageSection;
