

// import React, { useState } from 'react';
// import { Link } from 'react-router-dom';
// import '../Css/style.css';
// import Modal from 'react-modal';
// import Biocyll_Logo from '../Assets/Images/biocyll_Logo.png'
// const Footer = () => {
//   const phoneNumber = '918605200451';
//   const message = encodeURIComponent('Hello! I have a question.');
//   // Handle URL clicks for social media
//   const handleClick = (url) => {
//     window.open(url, '_blank');
//   };

//   return (
//     <>
//       <footer className="footer bg-light py-4">
//         <div className="container">
//           <div className="row">
//             <div className="col-md-3">
//               {/* <h5>Quick Links</h5> */}
//               <img src={Biocyll_Logo} style={{ width: '150px', height: 'auto',marginTop:'5px' }}/>
//             </div>
//             <div className="col-md-3 mb-3 text-justify">
//               {/* <h5>Our Products</h5> */}
//               <h5>Quick Links</h5>
//               <ul className="list-unstyled">
//                 <li><a href="" className="text-dark">About Us</a></li>
//                 <li><a href="" className="text-dark">Gallery</a></li>
//                 <li><a href="" className="text-dark">Contact Us</a></li>
//                 <li><a href="" className="text-dark">Blog</a></li>
//                 <li><a href="" className="text-dark">News</a></li>
//                 <li><a href="" className="text-dark">CSR</a></li>
//               </ul>
//             </div>
//             <div className="col-md-3 mb-3 text-justify">
//               <h5>Policy</h5>
//               <ul className="list-unstyled">
//                 <li><Link to="/privacypolicies" className="text-dark">Privacy Policy</Link></li>
//                 <li><Link to="/return" className="text-dark">Refund & Return Policy</Link></li>
//                 <li><Link to="/shipping" className="text-dark">Shipping & Delivery Policy</Link></li>
//                 <li><Link to="/terms-of-services" className="text-dark">Terms of Services</Link></li>
//               </ul>
//             </div>
//             <div className="col-md-3 mb-3 text-justify">
//               <h5>Contact</h5>
//               <p><a href="tel:18005322612" className="text-dark">18005322612</a></p>
//               <p><a href="mailto:enquiry@kaybeebio.com" className="text-dark">enquiry@kaybeebio.com</a></p>
//               <p>Kay Bee Bio Organics Pvt. Ltd., Office No. 208, 209 & 210. 2nd Floor, SPRINT Antaaya, Opposite Balewadi Stadium, Near Marvel Cascada, Balewadi, Pune, Maharashtra 411045</p>
//             </div>
//           </div>
//         </div>
//       </footer>
//       <div className="copyright-sec">
//         <div className="container">
//           <div className="copyright-wrap">
//             <div className="copiright-content">
//               <p>COPYRIGHT © 2024 - ALL RIGHTS RESERVED. MADE BY <a href="https://goanny.com/" target="_blank" rel="nofollow">Goanny Technologies</a></p>
//             </div>
//             <div className="social-links">
//               <a href="https://www.facebook.com/kaybeebioorganics" className="fb-icon" target="_blank">
//                 <i className="icon icon-facebook"></i>
//               </a>
//               <a href="https://www.instagram.com/kay_bee_bio_organic/" className="insta-icon" target="_blank">
//                 <i className="icon icon-instagram"></i>
//               </a>
//               <a href="https://www.linkedin.com/company/kay-bee-bio-organics-pvt-ltd" className="in-icon" target="_blank">
//                 <i className="icon icon-linkedin"></i>
//               </a>
//               <a href="https://twitter.com/kaybeebio" className="tw-icon" target="_blank">
//                 <i className="icon icon-twitter"></i>
//               </a>
//               <a href="https://www.youtube.com/channel/UCV4pzAmGsjm919MB6f48UBg" className="yt-icon" target="_blank">
//                 <i className="icon icon-youtube"></i>
//               </a>
//             </div>
//           </div>
//         </div>
//       </div>
//     </>
//   );
// };

// export default Footer;
import React from 'react';
import { Link } from 'react-router-dom';
import '../Css/style.css'; // Ensure your CSS is updated as shown below
import Biocyll_Logo from '../Assets/Images/biocyll_Logo.png';

const Footer = () => {
  return (
    <>
      <footer className="footer bg-light py-4">
        <div className="container">
          <div className="row">
            <div className="col-md-3 col-sm-6 col-12 text-center text-md-start mb-3">
              <img
                src={Biocyll_Logo}
                style={{ width: '150px', height: 'auto', marginTop: '5px' }}
                alt="Biocyll Logo"
              />
            </div>
            <div className="col-md-3 col-sm-6 col-12 mb-3 text-center text-md-start">
              <h5>Quick Links</h5>
              <ul className="list-unstyled">
                <li><a href="#" className="text-dark">About Us</a></li>
                <li><a href="#" className="text-dark">Gallery</a></li>
                <li><a href="#" className="text-dark">Contact Us</a></li>
                <li><a href="#" className="text-dark">Blog</a></li>
                <li><a href="#" className="text-dark">News</a></li>
                <li><a href="#" className="text-dark">CSR</a></li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-6 col-12 mb-3 text-center text-md-start">
              <h5>Policy</h5>
              <ul className="list-unstyled">
                <li><Link to="/privacypolicies" className="text-dark">Privacy Policy</Link></li>
                <li><Link to="/return" className="text-dark">Refund & Return Policy</Link></li>
                <li><Link to="/shipping" className="text-dark">Shipping & Delivery Policy</Link></li>
                <li><Link to="/terms-of-services" className="text-dark">Terms of Services</Link></li>
              </ul>
            </div>
            <div className="col-md-3 col-sm-6 col-12 mb-3 text-center text-md-start">
              <h5>Contact</h5>
              <p><a href="tel:18005322612" className="text-dark">18005322612</a></p>
              <p><a href="mailto:enquiry@kaybeebio.com" className="text-dark">enquiry@kaybeebio.com</a></p>
              <p>
                Kay Bee Bio Organics Pvt. Ltd., Office No. 208, 209 & 210. 2nd Floor,
                SPRINT Antaaya, Opposite Balewadi Stadium, Near Marvel Cascada, Balewadi,
                Pune, Maharashtra 411045
              </p>
            </div>
          </div>
        </div>
      </footer>
      <div className="copyright-sec">
        <div className="container d-flex flex-column flex-md-row justify-content-between align-items-center py-2">
          <p className="mb-2 mb-md-0 text-center">
            COPYRIGHT © 2024 - ALL RIGHTS RESERVED. MADE BY{' '}
            <a href="https://goanny.com/" target="_blank" rel="nofollow">Goanny Technologies</a>
          </p>
          <div className="social-links">
            <a href="https://www.facebook.com/kaybeebioorganics" target="_blank">
              <i className="icon icon-facebook"></i>
            </a>
            <a href="https://www.instagram.com/kay_bee_bio_organic/" target="_blank">
              <i className="icon icon-instagram"></i>
            </a>
            <a href="https://www.linkedin.com/company/kay-bee-bio-organics-pvt-ltd" target="_blank">
              <i className="icon icon-linkedin"></i>
            </a>
            <a href="https://twitter.com/kaybeebio" target="_blank">
              <i className="icon icon-twitter"></i>
            </a>
            <a href="https://www.youtube.com/channel/UCV4pzAmGsjm919MB6f48UBg" target="_blank">
              <i className="icon icon-youtube"></i>
            </a>
          </div>
        </div>
      </div>
    </>
  );
};

export default Footer;
