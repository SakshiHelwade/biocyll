// import React from 'react'
// import "../Css/style.css"

// const R_and_d = () => {
//   return (
//     <>
//       <section className='journey-section'>
//         <div class="container-fluid">
//           <div class="row">
//             <div class="col-md-12">
//               <div class="journey-text">
//                 <h3 className='mb-2'>Research and Development</h3>
//               </div>
//             </div>
//             <div className="col-md-6">
//               <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxepJDRf9DAt9m_u2niFxkFJF6sNUi-KxM4Q&s' />
//             </div>
//             <div className="col-md-6 journey-text">
//               <p>We specialize in pinpointing and extracting the most potent microbial strains from various niche sources, including soil, infected insects, plants, composts, and more. Upon rigorous validation, these strains are commercialized.</p>
//             </div>
//           </div>
//         </div>
//       </section>
//     </>
//   )
// }
// export default R_and_d
import React from 'react';
import AOS from 'aos';
import { useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'; // Import Bootstrap CSS
import 'aos/dist/aos.css'; // Import AOS CSS

const R_and_d = () => {
  useEffect(() => {
    AOS.init({
      duration: 1000, // Animation duration
    });
  }, []);
  
  return (
    <section className="journey py-5">
      <div className="container">
        <div className="row">
          {/* Heading */}
          <div className="col-12 text-center mb-4">
            <h1 className="text-success">
              Research & Development
            </h1>
          </div>

          {/* Image Section */}
          <div className="col-md-6 mb-4 mb-md-0" data-aos="fade-right">
            <img
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxepJDRf9DAt9m_u2niFxkFJF6sNUi-KxM4Q&s"
              className="img-fluid rounded"
              alt="Research and Development"
            />
          </div>

          {/* Text Section */}
          <div className="col-md-6 d-flex align-items-center" data-aos="fade-left">
            <p className="lead fs-5 fs-md-4 fs-sm-6">
              We specialize in pinpointing and extracting the most potent microbial strains from various niche sources,
              including soil, infected insects, plants, composts, and more. Upon rigorous validation, these strains
              are commercialized.
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default R_and_d;
