import React from 'react';
// import { Link, Element } from 'react-scroll';
import "../Css/style.css";
import about_img from '../Assets/Images/about4.jpg'
const About = () => {
  return (
    <>
      {/* <div className="row font-custom" style={{ fontSize: 20, fontWeight: "normal" }}>
        <div className="col-12 text-center mb-3 text-white pr-5 pl-5">
          <h3>Revolutionizing Sustainable Agriculture with Microbe Technology</h3>
        </div>
        <div className="col-12  pr-5 pl-5 ">
          <p>
            <div className='text-center pl-5 pr-5 pb-5'>
              <img src='https://media.istockphoto.com/id/1249522339/photo/tractor-spray-fertilizer-on-green-field.jpg?s=612x612&w=0&k=20&c=8tyljgHGgTAxrqKnpgsZVWsD-A2tsMRPFaqcErgV62o=' />
            </div>
            <div className='p-5 bg-white' style={{ width: '100%', height: 'auto', borderTopLeftRadius: 30, borderBottomRightRadius: 30, borderBottomLeftRadius: 30, textAlign:'justify' }}>
              At Biocyll Laboratories Pvt. Ltd., we celebrate 12 years of agricultural innovation with our groundbreaking Microbe Technology. This revolutionary advancement marks a significant shift in crop cultivation and product performance, reflecting our long-standing commitment to innovation in the Indian agricultural landscape.

              Central to this achievement is our state-of-the-art Technology Innovation Centre, where dedicated research and development have culminated in Microbe Technology. This technology leverages high-antagonism microbial strains to ensure extended shelf life and optimal performance of our products. The Microbe Technology Advantage is more than a technological breakthrough; it represents a revolution in farming.

              Our specially selected microbial strains enable our products to excel in challenging conditions, climate uncertainties, and varying soil types. They offer enhanced shelf life and improved quality, promoting resilience and productivity. Microbe Technology provides targeted benefits such as increased nutrient uptake, stronger disease resistance, and efficient pest management. It embodies sustainability by optimizing resource use and minimizing environmental impact.

              Join us in pioneering a brighter agricultural future with Microbe Technology. Help us unlock its full potential, ensuring higher yields, extended shelf life, and unparalleled product performance.
            </div>
          </p>
        </div>
      </div> */}
      <section>
  <div className='container-fluid'>
    <div className="row" style={{ backgroundColor: '#90d576', boxShadow: '0 -4px 20px 0 #90d576', textAlign: 'justify' }}>
      <div className="col-md-6 text-white">
        <h4 className="responsive-text"> 
          At Biocyll Laboratories Pvt. Ltd., we celebrate 12 years of agricultural innovation with our groundbreaking Microbe Technology. 
          This revolutionary advancement marks a significant shift in crop cultivation and product performance, reflecting our long-standing 
          commitment to innovation in the Indian agricultural landscape. Central to this achievement is our state-of-the-art Technology Innovation 
          Centre, where dedicated research and development have culminated in Microbe Technology.
          <br />
          This technology leverages high-antagonism microbial strains to ensure extended shelf life and optimal performance of our products. The Microbe Technology 
          Advantage is more than a technological breakthrough; it represents a revolution in farming.
          <br />
          Our specially selected microbial strains enable our products to excel in challenging conditions, climate uncertainties, and varying soil types. They offer enhanced 
          shelf life and improved quality, promoting resilience and productivity. Microbe Technology provides targeted benefits such as increased nutrient uptake, stronger 
          disease resistance, and efficient pest management. It embodies sustainability by optimizing resource use and minimizing environmental impact.
          <br />
          Join us in pioneering a brighter agricultural future with Microbe Technology. Help us unlock its full potential, ensuring higher yields, extended shelf life, and unparalleled product performance.
        </h4>
      </div>
      <div className="col-md-6">
        <img src='https://media.istockphoto.com/id/1249522339/photo/tractor-spray-fertilizer-on-green-field.jpg?s=612x612&w=0&k=20&c=8tyljgHGgTAxrqKnpgsZVWsD-A2tsMRPFaqcErgV62o=' className='h-10 mt-5' alt="Agriculture" />
      </div>
    </div>
  </div>
</section>

    </>
  )
}

export default About
