
import React from 'react';
import cert1 from '../Assets/cert1.jpg';
import cert2 from '../Assets/cert2.jpg';
import cert3 from '../Assets/cert3.jpg';
import "../Css/style.css";

const Certifications = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-12 text-center mb-4">
          <div className="bg-white p-5">
            <h1 className="mb-5 text-success">Certifications</h1>
            <p className="lead"  data-aos="fade-in">
              An ISO 9001:2015 certified for Quality Management Systems, we ensure
              the highest standards for quality and safety in the manufacturing of our
              Biofertilizers, Bio-fungicides, Bio-insecticides, and Bio-stimulants.
            </p>
          </div>
        </div>
        <div className="col-12">
          <div className="row">
            <div className="col-12 col-md-6 mb-4" data-aos="fade-right">
              <img src={cert3} className="img-fluid" alt="Certification 3" />
            </div>
            <div className="col-12 col-md-6 d-flex flex-column justify-content-center" data-aos="fade-left">
              <div className="mb-4">
                <img src={cert1} className="img-fluid" alt="Certification 1" />
              </div>
              <div className="d-flex justify-content-center">
                <img src={cert2} className="img-fluid rotate-img" alt="Certification 2" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Certifications;
