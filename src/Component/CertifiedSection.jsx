
// import React, { useContext, useState } from 'react';
// import { UserContext } from '../Context/CreateContext';
// import axios from 'axios';
// import { base_url } from '../Config/Index';
// import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import "../Css/style.css"

// const ContactForm = () => {
//     const { user, token } = useContext(UserContext);
//     const [contact, setContact] = useState({
//         Name: "",
//         Email: "",
//         Phone: "",
//         Company: "",
//         Occupation: "",
//         Message: "",
//     });

//     const handleChange = (event) => {
//         setContact({ ...contact, [event.target.name]: event.target.value });
//     };

//     const handleRadioChange = (event) => {
//         setContact({ ...contact, Occupation: event.target.value });
//     };

//     const validateForm = () => {
//         const { Name, Email, Phone, Company, Message } = contact;

//         if (!Name || !Email || !Phone || !Company || !Message || !contact.Occupation) {
//             toast.error("Please fill out all fields.");
//             return false;
//         }

//         const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//         if (!emailPattern.test(Email)) {
//             toast.error("Please enter a valid email address.");
//             return false;
//         }

//         const phonePattern = /^\+?\d{10,15}$/;
//         if (!phonePattern.test(Phone)) {
//             toast.error("Please enter a valid phone number.");
//             return false;
//         }

//         return true;
//     };

//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         if (!validateForm()) {
//             return;
//         }

//         const payload = {
//             Name: contact.Name,
//             Email: contact.Email,
//             Phone: contact.Phone,
//             Company: contact.Company,
//             Occupation: contact.Occupation,
//             Message: contact.Message,
//         };

//         try {
//             const response = await axios.post(`${base_url}/api/contactus/post`, payload, {
//                 headers: {
//                     Authorization: `${token}`,
//                 },
//             });
//             console.log(response.data);
//             toast.success("Form submitted successfully!");
//             setContact({
//                 Name: "",
//                 Email: "",
//                 Phone: "",
//                 Company: "",
//                 Occupation: "",
//                 Message: "",
//             });
//         } catch (error) {
//             console.log(error);
//             toast.error("Failed to submit the form. Please try again.");
//         }
//     };

//     return (
//         <>
//             <section className="contact-section">
//                 <div className="container-fluid">
//                     <div className="row">
//                         <div className="col-12 bg-light text-center">
//                             <h3><b>Get in Touch</b></h3>
//                         </div>

//                         <div className="contact-info-wrapper">
//                             <div className="col-12 p-3 text-center">
//                                 <h5 className='text-white'>Reach out to us for any inquiries or information you require that isn't available here, and we'll respond promptly.</h5>
//                             </div>

//                             <div className="row p-3">
//                                 <div className="col-md-5 contact-info" style={{ color: 'white' }}>
//                                     <p><b>Phone Number :</b><br />+91 20 60202626</p>
//                                     <p><b>Email :</b><br />info@biocyll.com</p>
//                                     <p><b>Office Address :</b><br />Office No 404, Karle Chowk, Karle Empire, Nanded City Phase 2, Sinhagad Road Pune 411041</p>
//                                     <p><b>Mfg Address :</b><br />Sr. No 168/3B Near Sakal Press, Behind Laxmi Vajan Kata, Uruli Devachi, Tal. Haveli, Dist. Pune - 412308</p>
//                                 </div>

//                                 <div className="col-md-7 bg-light p-4">
//                                     <form onSubmit={handleSubmit}>
//                                         <div className="row">
//                                             <div className="col-md-6 col-sm-12 mt-3">
//                                                 Full Name :
//                                                 <input placeholder='Enter Your Name' className='form-control' name='Name' value={contact.Name} onChange={handleChange} />
//                                             </div>

//                                             <div className="col-md-6 col-sm-12 mt-3">
//                                                 Email :
//                                                 <input placeholder='Enter Your Email' className='form-control' name='Email' value={contact.Email} onChange={handleChange} />
//                                             </div>

//                                             <div className="col-md-6 col-sm-12 mt-3">
//                                                 Phone :
//                                                 <input placeholder='Enter Your Phone Number' className='form-control' name='Phone' value={contact.Phone} onChange={handleChange} />
//                                             </div>

//                                             <div className="col-md-6 col-sm-12 mt-3">
//                                                 Company Name :
//                                                 <input placeholder='Enter Your Company Name' className='form-control' name='Company' value={contact.Company} onChange={handleChange} />
//                                             </div>

//                                             <div className="col-12 mt-4">
//                                                 <h5>User Type :</h5>
//                                             </div>

//                                             <div className="d-flex align-items-left mt-3 col-12">
//                                                 <div className="form-check form-check-inline mr-4">
//                                                     <input
//                                                         type="radio"
//                                                         id="business"
//                                                         name="occupation"
//                                                         className="form-check-input"
//                                                         value="Business Enquiry"
//                                                         checked={contact.Occupation === "Business Enquiry"}
//                                                         onChange={handleRadioChange}
//                                                     />
//                                                     <label htmlFor="business" className="ml-1">Corporate</label>
//                                                 </div>

//                                                 <div className="form-check form-check-inline">
//                                                     <input
//                                                         type="radio"
//                                                         id="farmer"
//                                                         name="occupation"
//                                                         className="form-check-input"
//                                                         value="Farmer Enquiry"
//                                                         checked={contact.Occupation === "Farmer Enquiry"}
//                                                         onChange={handleRadioChange}
//                                                     />
//                                                     <label htmlFor="farmer" className="ml-1">Farmer</label>
//                                                 </div>
//                                             </div>

//                                             <div className='col-12 mt-3'>
//                                                 Message :
//                                                 <textarea placeholder='Message' className='form-control' name='Message' value={contact.Message} onChange={handleChange} style={{ height: '200px' }} />
//                                             </div>

//                                             <div className='col-12 mt-3 text-center'>
//                                                 <button type='submit' className='btn btn-success btn-lg'>Submit</button>
//                                             </div>
//                                         </div>
//                                     </form>
//                                 </div>

//                             </div>
//                         </div>

//                     </div>
//                 </div>
//             </section>

//             <ToastContainer />
//         </>
//     );
// };

// export default ContactForm;
import React from 'react';

const CertifiedSection = () => {
  return (
    <section className="certified-sec">
      <div className="container">
        <div className="certified-wrap" style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
          <div className="certified-left aos-init aos-animate" data-aos="fade-up" style={{ flex: 1 }}>
            <h3 className="section-heading">Certified by</h3>
            <div className="row">
              <div
                className="certified-logo-slider"
                style={{
                  display: 'flex',
                  overflowX: 'auto',
                  gap: '20px',
                  padding: '10px 0',
                }}
              >
                <div className="certified-logo-slide" style={{ flexShrink: 0 }}>
                  <figure>
                    <img
                      src="https://kaybeebio.com/wp-content/uploads/2023/05/certified-img-1.png"
                      className="img-responsive"
                      alt="certified img 1"
                      width="118"
                      height="127"
                      loading="lazy"
                    />
                  </figure>
                </div>
                <div className="certified-logo-slide" style={{ flexShrink: 0 }}>
                  <figure>
                    <img
                      src="https://kaybeebio.com/wp-content/uploads/2023/05/certified-img-2.png"
                      className="img-responsive"
                      alt="certified img 2"
                      width="145"
                      height="93"
                      loading="lazy"
                    />
                  </figure>
                </div>
                <div className="certified-logo-slide" style={{ flexShrink: 0 }}>
                  <figure>
                    <img
                      src="https://kaybeebio.com/wp-content/uploads/2023/05/certified-img-3.png"
                      className="img-responsive"
                      alt="certified img 3"
                      width="114"
                      height="115"
                      loading="lazy"
                    />
                  </figure>
                </div>
                <div className="certified-logo-slide" style={{ flexShrink: 0 }}>
                  <figure>
                    <img
                      src="https://kaybeebio.com/wp-content/uploads/2023/05/certified-img-4.png"
                      className="img-responsive"
                      alt="certified img 4"
                      width="133"
                      height="113"
                      loading="lazy"
                    />
                  </figure>
                </div>
              </div>
            </div>
          </div>

          <div className="certified-right aos-init aos-animate" data-aos="fade-up" style={{ flex: 1, position: 'relative' }}>
            <div className="advisory-serv-block" style={{ position: 'relative' }}>
              <div
                className="consultant-image"
                style={{
                  position: 'relative',
                  width: '100%',
                  height: '500px',
                  backgroundImage:
                    'url("https://media.istockphoto.com/id/1311655328/photo/im-the-best-asset-in-my-business.jpg?s=612x612&w=0&k=20&c=ebkVt_iY6rRjXvyx2CESPC8EtcWrv0nYt_y4IdKmN3M=")',
                  backgroundSize: 'cover', // Ensure the image is properly resized
                  backgroundPosition: 'center',
                  backgroundRepeat: 'no-repeat',
                }}
              >
                <div
                  className="advisory-serv-head"
                  style={{
                    position: 'absolute',
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    textAlign: 'left',
                    zIndex: 1,
                    color: 'red',
                  }}
                >
                  <h3>
                    <b>Agri</b>
                    <br />
                    Advisory Services
                  </h3>
                  <p>To get free consultation, drop your number.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        /* Ensure the background image fits well on small screens */
        @media (max-width: 768px) {
          .consultant-image {
            height: 300px; /* Adjust height for mobile view */
            background-size: cover; /* Adjust background size */
            background-position: center; /* Keep the image centered */
          }
        }
      `}</style>
    </section>
  );
};

export default CertifiedSection;

