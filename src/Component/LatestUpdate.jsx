// import { useEffect } from "react";
// import React from 'react'
// import "../Css/style.css";
// function LatestUpdate() {
//     // Function to handle fade-in effect on scroll
//     const handleScrollFadeIn = () => {
//       const elements = document.querySelectorAll('.fade-in');

//       elements.forEach(element => {
//         const rect = element.getBoundingClientRect();
//         if (rect.top <= window.innerHeight && rect.bottom >= 0) {
//           // Add fade-in-visible class when the element is in view
//           element.classList.add('fade-in-visible');
//         }
//       });
//     };

//     useEffect(() => {
//       // Add scroll event listener
//       window.addEventListener('scroll', handleScrollFadeIn);

//       // Call it once to check elements initially
//       handleScrollFadeIn();

//       // Clean up the event listener on component unmount
//       return () => {
//         window.removeEventListener('scroll', handleScrollFadeIn);
//       };
//     }, []);
//       return (
//     <div className="container my-5">
//       <div className="row">
//         <div className="col-12 text-center mb-4">
//           <h3 className="section-heading">Latest updates</h3>
//           <a href="https://kaybeebio.com/blog/" className="btn btn-primary">View all latest updates</a>
//         </div>

//         <div id="latestUpdatesSlider" className="carousel slide" data-bs-ride="carousel">
//           <div className="carousel-inner">

//             <div className="carousel-item active">
//               <div className="row">
//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/the-role-of-ai-and-robotics-in-modern-farming/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/07/AI-and-Robotics-in-Modern-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="AI and Robotics in Modern Farming Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">The Role of AI and Robotics in Modern Farming</h5>
//                         <p className="card-text">Introduction: Modern farming is experiencing a significant transformation due to advancements in technology. Among th</p>
//                         <span className="text-muted">July 29, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>

//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/world-nature-conservation-day-pollinators-matter/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/07/Sustainable-Farming-Practices-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Practices Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">World Nature Conservation Day: Pollinators Matter</h5>
//                         <p className="card-text">There’s a saying, “Life is an echo, what you do, comes back to you.” This is the condition of humans in the presen</p>
//                         <span className="text-muted">July 27, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>

//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/going-organic-how-it-benefits-the-environment/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/08/Organic-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Organic Farming Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">Going Organic: How it Benefits the Environment</h5>
//                         <p className="card-text">The world organic food market has undergone significant expansion, with an increase of 11.6% from 2023 to 2024 and a pro</p>
//                         <span className="text-muted">August 27, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>
//               </div>
//             </div>

//             {/* Second Item */}
//             <div className="carousel-item">
//               <div className="row">
//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/sustainable-farming-practices-to-mitigate-climate-change/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/08/Sustainable-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">Sustainable Farming Practices to Mitigate Climate Change</h5>
//                         <p className="card-text">Climate change poses one of the most significant challenges of our time, affecting ecosystems, weather patterns, and agr</p>
//                         <span className="text-muted">August 2, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>

//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/the-role-of-ai-and-robotics-in-modern-farming/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/07/AI-and-Robotics-in-Modern-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="AI and Robotics in Modern Farming Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">The Role of AI and Robotics in Modern Farming</h5>
//                         <p className="card-text">Introduction: Modern farming is experiencing a significant transformation due to advancements in technology. Among th</p>
//                         <span className="text-muted">July 29, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>

//                 <div className="col-md-4">
//                   <div className="card">
//                     <a href="https://kaybeebio.com/blog/world-nature-conservation-day-pollinators-matter/">
//                       <img src="https://kaybeebio.com/wp-content/uploads/2024/07/Sustainable-Farming-Practices-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Practices Kay Bee Bio" />
//                       <div className="card-body">
//                         <h5 className="card-title">World Nature Conservation Day: Pollinators Matter</h5>
//                         <p className="card-text">There’s a saying, “Life is an echo, what you do, comes back to you.” This is the condition of humans in the presen</p>
//                         <span className="text-muted">July 27, 2024</span>
//                       </div>
//                     </a>
//                   </div>
//                 </div>
//               </div>
//             </div>

//           </div>

//           <button className="carousel-control-prev" type="button" data-bs-target="#latestUpdatesSlider" data-bs-slide="prev">
//             <span className="carousel-control-prev-icon" aria-hidden="true"></span>
//             <span className="visually-hidden">Previous</span>
//           </button>
//           <button className="carousel-control-next" type="button" data-bs-target="#latestUpdatesSlider" data-bs-slide="next">
//             <span className="carousel-control-next-icon" aria-hidden="true"></span>
//             <span className="visually-hidden">Next</span>
//           </button>

//           <div className="carousel-indicators">
//             <button type="button" data-bs-target="#latestUpdatesSlider" data-bs-slide-to="0" className="active" aria-current="true"></button>
//             <button type="button" data-bs-target="#latestUpdatesSlider" data-bs-slide-to="1"></button>
//           </div>
//         </div>
//       </div>
//     </div>
//   )
// }

// export default LatestUpdate
import { useEffect } from "react";
import React from 'react';
import "../Css/style.css";

function LatestUpdate() {
  // Function to handle fade-in effect on scroll
  const handleScrollFadeIn = () => {
    const elements = document.querySelectorAll('.fade-in');

    elements.forEach(element => {
      const rect = element.getBoundingClientRect();
      if (rect.top <= window.innerHeight && rect.bottom >= 0) {
        // Add fade-in-visible class when the element is in view
        element.classList.add('fade-in-visible');
      }
    });
  };

  useEffect(() => {
    // Add scroll event listener
    window.addEventListener('scroll', handleScrollFadeIn);

    // Call it once to check elements initially
    handleScrollFadeIn();

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener('scroll', handleScrollFadeIn);
    };
  }, []);




  return (
    <div className="container my-5">
      <div className="row">
        <div className="col-12 text-center mb-4">
          <h3 className="section-heading">Latest updates</h3>
          {/* <a href="https://kaybeebio.com/blog/" className="btn btn-primary">View all latest updates</a> */}
        </div>

        <div id="latestUpdatesSlider" className="carousel slide" data-bs-ride="carousel">
          <div className="carousel-inner">

            <div className="carousel-item active">
              <div className="row">
                <div className="col-md-4 fade-in">
                  <div className="card">
                    <a href="/">
                      <img src="https://kaybeebio.com/wp-content/uploads/2024/07/Sustainable-Farming-Practices-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Practices Kay Bee Bio" />
                      <div className="card-body">
                        <h5 className="card-title">The Role of AI and Robotics in Modern Farming</h5>
                        <p className="card-text">Introduction: Modern farming is experiencing a significant transformation due to advancements in technology. Among th</p>
                        <span className="text-muted">July 29, 2024</span>
                      </div>
                    </a>
                  </div>
                </div>

                <div className="col-md-4 fade-in">
                  <div className="card">
                    <a href="/">
                      <img src="https://kaybeebio.com/wp-content/uploads/2024/07/Sustainable-Farming-Practices-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Practices Kay Bee Bio" />
                      <div className="card-body">
                        <h5 className="card-title">World Nature Conservation Day: Pollinators Matter</h5>
                        <p className="card-text">There’s a saying, “Life is an echo, what you do, comes back to you.” This is the condition of humans in the presen</p>
                        <span className="text-muted">July 27, 2024</span>
                      </div>
                    </a>
                  </div>
                </div>

                <div className="col-md-4 fade-in">
                  <div className="card">
                    <a href="">
                      <img src="https://kaybeebio.com/wp-content/uploads/2024/08/Organic-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Organic Farming Kay Bee Bio" />
                      <div className="card-body">
                        <h5 className="card-title">Going Organic: How it Benefits the Environment</h5>
                        <p className="card-text">The world organic food market has undergone significant expansion, with an increase of 11.6% from 2023 to 2024 and a pro</p>
                        <span className="text-muted">August 27, 2024</span>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>

            {/* Second Item */}
            <div className="carousel-item">
              <div className="row">
                <div className="col-md-4 fade-in">
                  <div className="card">
                    <a href="/">
                      <img src="https://kaybeebio.com/wp-content/uploads/2024/08/Sustainable-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Kay Bee Bio" />
                      <div className="card-body">
                        <h5 className="card-title">Sustainable Farming Practices to Mitigate Climate Change</h5>
                        <p className="card-text">Climate change poses one of the most significant challenges of our time, affecting ecosystems, weather patterns, and agr</p>
                        <span className="text-muted">August 2, 2024</span>
                      </div>
                    </a>
                  </div>
                </div>

                <div className="col-md-4 fade-in">
                  <div className="card">
                    {/* <a href="https://kaybeebio.com/blog/the-role-of-ai-and-robotics-in-modern-farming/"> */}
                    <img src="https://kaybeebio.com/wp-content/uploads/2024/07/AI-and-Robotics-in-Modern-Farming-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="AI and Robotics in Modern Farming Kay Bee Bio" />
                    <div className="card-body">
                      <h5 className="card-title">The Role of AI and Robotics in Modern Farming</h5>
                      <p className="card-text">Introduction: Modern farming is experiencing a significant transformation due to advancements in technology. Among th</p>
                      <span className="text-muted">July 29, 2024</span>
                    </div>
                    {/* </a> */}
                  </div>
                </div>

                <div className="col-md-4 fade-in">
                  <div className="card">
                    {/* <a href="https://kaybeebio.com/blog/world-nature-conservation-day-pollinators-matter/"> */}
                    <img src="https://kaybeebio.com/wp-content/uploads/2024/07/Sustainable-Farming-Practices-Kay-Bee-Bio-290x220.jpg" className="card-img-top" alt="Sustainable Farming Practices Kay Bee Bio" />
                    <div className="card-body">
                      <h5 className="card-title">World Nature Conservation Day: Pollinators Matter</h5>
                      <p className="card-text">There’s a saying, “Life is an echo, what you do, comes back to you.” This is the condition of humans in the presen</p>
                      <span className="text-muted">July 27, 2024</span>
                    </div>
                    {/* </a> */}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LatestUpdate;
