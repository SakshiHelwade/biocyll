// import React from 'react';
// import { Link, useNavigate } from 'react-router-dom';
// import "../Css/style.css";
// import slide1 from "../Assets/Images/slide1.jpg";
// import Img2 from "../Assets/Images/Img2.jpeg";
// import Img3 from "../Assets/Images/Img3.jpeg";
// import Img4 from "../Assets/Images/Img4.jpeg";
// const Slider = () => {
//     const navigate = useNavigate();
//     return (
//         <section>
//             <div className="container-fluid">
//                 <div className="row">
//                     <div className="col-md-12">
//                         <div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel" data-bs-interval="3000">
//                             {/* <div className="carousel-indicators">
//                                 <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
//                                 <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
//                                 <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
//                                 <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 4"></button>
//                             </div> */}

//                             <div className="carousel-inner">
//                                 <div className="carousel-item active">
//                                     <img
//                                         src={slide1}
//                                         className="d-block w-100"
//                                         style={{ height: "auto", maxHeight: "510px" }}
//                                         alt="Experience"
//                                     />
//                                     <div className="carousel-caption d-none d-md-block">
//                                         <h1 className="display-4" style={{ color: "black", backgroundColor: "rgba(255, 255, 255, 0.5)" }}>12 years of experience</h1>
//                                         <p
//                                             className="btn btn-light btn-lg mt-3"
//                                         >
//                                             <Link to="/explore/experience">Explore More</Link>
//                                         </p>
//                                     </div>
//                                 </div>

//                                 <div className="carousel-item">
//                                     <img
//                                         src={Img2}
//                                         className="d-block"
//                                         style={{ height: "auto", maxHeight: "500px", width: "100%" }}
//                                         alt="Revamping Agriculture"
//                                     />
//                                     <div className="carousel-caption d-none d-md-block text-center">
//                                         <h1 className="display-4" style={{ color: "black", backgroundColor: "rgba(255, 255, 255, 0.5)" }}>Revamping agriculture with Microbial solutions</h1>
//                                         <button
//                                             className="btn btn-light btn-lg mt-3"
//                                             onClick={() => navigate('/explore/revamping')}
//                                         >
//                                             Explore More
//                                         </button>
//                                     </div>
//                                 </div>
//                                 <div className="carousel-item">
//                                     <img
//                                         src={Img3}
//                                         className="d-block w-100"
//                                         style={{ height: "auto", maxHeight: "500px" }}
//                                         alt="Innovation Facility"
//                                     />
//                                     <div className="carousel-caption d-none d-md-block text-center">
//                                         <h1 className="display-4" style={{ color: "black", backgroundColor: "rgba(255, 255, 255, 0.5)" }}>Our premier technology and innovation facility</h1>
//                                         <button
//                                             className="btn btn-light btn-lg mt-3"
//                                             onClick={() => navigate('/explore/ourPremier')}
//                                         >
//                                             Explore More
//                                         </button>
//                                     </div>
//                                 </div>
//                                 <div className="carousel-item">
//                                     <img
//                                         src={Img4}
//                                         className="d-block w-100"
//                                         style={{ height: "auto", maxHeight: "500px" }}
//                                         alt="Values"
//                                     />
//                                     <div className="carousel-caption d-none d-md-block text-center">
//                                         <h1 className="display-4" style={{ color: "black", backgroundColor: "rgba(255, 255, 255, 0.5)" }}>Our Values</h1>
//                                         <button
//                                             className="btn btn-light btn-lg mt-3"
//                                             onClick={() => navigate('/explore/values')}
//                                         >
//                                             Explore More
//                                         </button>
//                                     </div>
//                                 </div>
//                             </div>

//                             <button className="carousel-control-prev" type="button" data-target="#carouselExampleCaptions" data-slide="prev">
//                                 <span className="carousel-control-prev-icon" aria-hidden="true"></span>
//                                 <span className="visually-hidden">Previous</span>
//                             </button>
//                             <button className="carousel-control-next" type="button" data-target="#carouselExampleCaptions" data-slide="next">
//                                 <span className="carousel-control-next-icon" aria-hidden="true"></span>
//                                 <span className="visually-hidden">Next</span>
//                             </button>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </section>
//     );
// };

// export default Slider;
// import React, { useState, useEffect } from "react";

// const Slider = () => {
//     const [selectedBox, setSelectedBox] = useState("box1"); // Initially set to box1
//     const boxKeys = ["box1", "box2", "box3", "box4"]; // Keys for boxes

//     // Text mapping for boxes
//     const textContent = {
//         box1: { title: "RESULTS IN 48 HOURS", description: "Our products start showing results in 48 hours." },
//         box2: { title: "GROWTH IN HOURS", description: "Achieve noticeable growth in just 72 hours." },
//         box3: { title: "PATENT PRODUCTS", description: "Patented Products ensure quality and trust." },
//         box4: { title: "GRANULES PRODUCTS", description: "Granule-based products for long-lasting results." },
//     };

    
//     // Automatically change the selected box every 3 seconds
//     useEffect(() => {
//         const interval = setInterval(() => {
//             setSelectedBox((prevBox) => {
//                 const currentIndex = boxKeys.indexOf(prevBox);
//                 const nextIndex = (currentIndex + 1) % boxKeys.length;
//                 return boxKeys[nextIndex];
//             });
//         }, 3000);

//         return () => clearInterval(interval); // Cleanup interval on component unmount
//     }, [boxKeys]);

//     return (
//         <div
//     className="container-fluid text-center py-5 w-100"
//     style={{
//         backgroundImage: "url('https://media.istockphoto.com/id/951705140/photo/alley-of-ripe-pomegranate-fruits-hanging-on-a-tree-branches-in-the-garden-harvest-concept.jpg?s=612x612&w=0&k=20&c=bD5SrgKXoIqO8CdUMwzTn3RCXtoQ23zzgLuh-Haagr4=')",
//         backgroundSize: "cover",
//         backgroundPosition: "center",
//         backgroundRepeat: "no-repeat",
//     }}
// >
//         <div className="container-fluid text-center py-5 w-100">
//             <div className="container-fluid text-center py-5 w-100">
//                 {/* Additional Text */}
//                 <div className="mb-3">
//                     <h4 className="text-uppercase fw-semibold">Explore Our Product Features</h4>
//                     <p className="text-muted">Click on the boxes below to see detailed information about each feature.</p>
//                 </div>
//                 {/* Display Selected Text */}
//                 <div className="mb-4 text-start">
//                     {selectedBox && (
//                         <div>
//                             <h2 className="fw-bold text-uppercase">{textContent[selectedBox].title}</h2>
//                             <p className="text-muted">{textContent[selectedBox].description}</p>
//                         </div>
//                     )}
//                 </div>

//                 {/* Boxes */}
//                 <div className="row">
//                     {boxKeys.map((boxKey, index) => (
//                         <div key={index} className="col-md-3">
//                             <div
//                                 className={`p-4 border rounded text-center ${selectedBox === boxKey ? "bg-dark text-white" : "bg-light"
//                                     }`}
//                                 onClick={() => setSelectedBox(boxKey)}
//                                 style={{ cursor: "pointer" }}
//                             >
//                                 <h3 className="fw-bold">
//                                     {boxKey === "box1" ? "48" : boxKey === "box2" ? "72" : boxKey === "box3" ? "Patent" : "Granules"}
//                                 </h3>
//                                 <p>{boxKey === "box1" ? "RESULT IN HOURS" : boxKey === "box2" ? "GROWTH IN HOURS" : "PRODUCTS"}</p>
//                             </div>
//                         </div>
//                     ))}
//                 </div>
//             </div>
//         </div>
//         </div>
//     );
// };

// export default Slider;
import React, { useState, useEffect } from "react";

const Slider = () => {
    const [selectedBox, setSelectedBox] = useState("box1"); // Initially set to box1
    const boxKeys = ["box1", "box2", "box3", "box4"]; // Keys for boxes

    // Text mapping for boxes
    const textContent = {
        box1: { title: "RESULTS IN 48 HOURS", description: "Our products start showing results in 48 hours." },
        box2: { title: "GROWTH IN HOURS", description: "Achieve noticeable growth in just 72 hours." },
        box3: { title: "PATENT PRODUCTS", description: "Patented Products ensure quality and trust." },
        box4: { title: "GRANULES PRODUCTS", description: "Granule-based products for long-lasting results." },
    };

    // Background images for each box
    const backgroundImages = {
        box1: "https://media.istockphoto.com/id/951705140/photo/alley-of-ripe-pomegranate-fruits-hanging-on-a-tree-branches-in-the-garden-harvest-concept.jpg?s=612x612&w=0&k=20&c=bD5SrgKXoIqO8CdUMwzTn3RCXtoQ23zzgLuh-Haagr4=",
        box2: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8dguc-gmmfpM-Rzw55fEn_cBhjtZeUig8hw&s",
        box3: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcReheZWypOTewuQ20vWccgs3VJga8zZbapEsw&s",
        box4: "https://www.shutterstock.com/image-photo/copy-space-wood-branch-green-260nw-2131300369.jpg",
    };

    // Automatically change the selected box every 3 seconds
    useEffect(() => {
        const interval = setInterval(() => {
            setSelectedBox((prevBox) => {
                const currentIndex = boxKeys.indexOf(prevBox);
                const nextIndex = (currentIndex + 1) % boxKeys.length;
                return boxKeys[nextIndex];
            });
        }, 3000);

        return () => clearInterval(interval); // Cleanup interval on component unmount
    }, [boxKeys]);

    return (
        <div
            className="container-fluid text-center py-5 w-100"
            style={{
                backgroundImage: `url(${backgroundImages[selectedBox]})`,
                backgroundSize: "cover",
                backgroundPosition: "center",
                backgroundRepeat: "no-repeat",
                transition: "background-image 1s ease-in-out", // Smooth transition effect
            }}
        >
            <div className="container-fluid text-center py-5 w-100">
                {/* Additional Text */}
                <div className="mb-3">
                    <h4 className="text-uppercase fw-semibold">Explore Our Product Features</h4>
                    <p className="text-muted">Click on the boxes below to see detailed information about each feature.</p>
                </div>
                {/* Display Selected Text */}
                <div className="mb-4 text-start">
                    {selectedBox && (
                        <div>
                            <h2 className="fw-bold text-uppercase">{textContent[selectedBox].title}</h2>
                            <p className="text-muted">{textContent[selectedBox].description}</p>
                        </div>
                    )}
                </div>

                {/* Boxes */}
                <div className="row">
                    {boxKeys.map((boxKey, index) => (
                        <div key={index} className="col-md-3">
                            <div
                                className={`p-4 border rounded text-center ${selectedBox === boxKey ? "bg-dark text-white" : "bg-light"
                                    }`}
                                onClick={() => setSelectedBox(boxKey)}
                                style={{ cursor: "pointer" }}
                            >
                                <h3 className="fw-bold">
                                    {boxKey === "box1" ? "48" : boxKey === "box2" ? "72" : boxKey === "box3" ? "Patent" : "Granules"}
                                </h3>
                                <p>{boxKey === "box1" ? "RESULT IN HOURS" : boxKey === "box2" ? "GROWTH IN HOURS" : "PRODUCTS"}</p>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Slider;
