// import React from 'react'
// import "../Css/style.css"
// const Production_fac = () => {
//   return (
//     <section className='journey-section'>
//       <div class="container-fluid">
//         <div class="row">
//           <div className="col-md-6">
//             <img src='http://iplbiologicals.com/wp-content/uploads/2023/09/Production-Technology.jpg'/>
//           </div>
//           <div class="col-md-6">
//             <div class="journey-text">
//               <h3 className='mb-2'>Production Facilities</h3>
//               <p> The manufacturing facility of Biocyll Laboratories Pvt. Ltd. in Pune, is furnished with contemporary, cutting-edge machinery, which includes bioreactors and associated utilities, an aseptic filling unit, as well as downstream equipment tailored for the production of liquid, powder, and granule formulations.</p>
//             </div>
//           </div>
//         </div>
//       </div>
//     </section>
//   )
// }

// export default Production_fac
import React from 'react';
// import "../Css/style.css";
import AOS from 'aos'
import { useEffect } from 'react';

const Production_fac = () => {
  useEffect(() => {
    AOS.init({
      duration: 1000, // Animation duration
    })
  }, [])
  return (
    <section className='journey'>
      <div className="container-fluid">
        <div className="row">
          <div className='col-md-12'>
            <h1 className='mb-2 text-success text-center'>Production Facility</h1><br />
          </div>
          <div className="col-md-6" data-aos="zoom-in">
          <p style={{ marginTop: '20px', }}>The manufacturing facility of Biocyll Laboratories Pvt. Ltd. in Pune, is furnished with contemporary, cutting-edge machinery, which includes bioreactors and associated utilities, an aseptic filling unit, as well as downstream equipment tailored for the production of liquid, powder, and granule formulations.</p>

          
          </div>
          <div className="col-md-6" >
            <div className="journey-text" data-aos="fade-left">
            <img
              src='https://www.spanco.com/wp-content/uploads/2014/09/01-Manufacturing-Facility-Layout-Plan.jpg'
              className='img-fluid'
              alt='Production Facilities'
              style={{ maxWidth: '100%', height: 'auto'}}
            />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Production_fac;
