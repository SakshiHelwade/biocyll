// // import React from 'react';
// import "../Css/style.css";
// import performance from "../Assets/Images/performance.png";
// import s3 from "../Assets/Images/s3.png";
// import s4 from "../Assets/Images/s4.png";
// import s5 from "../Assets/Images/s5.png";
// import s6 from "../Assets/Images/s6.png";
// import s7 from "../Assets/Images/s7.png";

// const Services = () => {
//     return (
//         <section>
//             <div className="container-fluid p-5">
//                 {/* <div className="col-12 pt-2 text-center">
//                     <p style={{ fontSize: 40 }}>Our Facts</p>
//                     <h4>Shaping Today's Actions for a Greener Future</h4>
//                 </div> */}
//                 <div className="col-12 pt-2 text-center">
//     <p className="our-facts-title">Our Facts</p>
//     <h4 className="our-facts-subtitle">Shaping Today's Actions for a Greener Future</h4>
// </div>
//                 <div style={{ backgroundColor: "#90d576", boxShadow: '0 -4px 20px 0 #90d576', width: "100%" }}>
//                     {/* First Row */}
//                     <div className="row mt-5 p-5">
//                         <div className="col-12 col-md-6 col-lg-3 mb-4">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={performance} alt="Performance" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>We conduct ourselves and our business affairs in accordance with the highest ethical standards.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>

//                         <div className="col-12 col-md-6 col-lg-3 mb-4">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={s3} alt="Maximize Success" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>We base our decisions on maximizing the long-term success of our company.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>

//                         <div className="col-12 col-md-6 col-lg-3 mb-4">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={s4} alt="Competitive Success" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>Each individual doing what it takes to win" suggests a competitive environment where success is prioritized.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>

//                         <div className="col-12 col-md-6 col-lg-3 mb-4">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={s5} alt="Customer Focused" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>We will work towards a customer-focused organization, keeping the customer front and center in all we do.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>

//                     {/* Second Row */}
//                     <div className="row  p-5">
//                         <div className="col-12 col-md-6 col-lg-3 mb-4 ">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={s6} alt="Customer Front" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>We will work towards a customer-focused organization, keeping the customer front and center in all we do.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>

//                         <div className="col-12 col-md-6 col-lg-3 mb-4">
//                             <div className="card card_3d">
//                                 <div className="card-inner">
//                                     <div className="card-front p-5 text-center">
//                                         <img src={s7} alt="Quality Responsibility" className="img-fluid w-75" />
//                                     </div>
//                                     <div className="card-back p-3 text-center">
//                                         <p>We take responsibility for quality. Our product and services will be amongst the best in terms of value delivered.</p>
//                                     </div>
//                                 </div>
//                             </div>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </section>
//     );
// };

// export default Services;

// import React, { useEffect, useState } from "react";
// import "bootstrap/dist/css/bootstrap.min.css";

// const categories = [
//   {
//     imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/downy-raze-4-300x300.jpg",
//     alt: "Bio Pesticides Products",
//     title: "Bio Pesticides Products",
//     href: "https://kaybeebio.com/product-category/bio-pesticides/",
//   },
//   {
//     imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/balanstick-1-300x300.jpg",
//     alt: "Stickers & Spreaders",
//     title: "Stickers & Spreaders",
//     href: "https://kaybeebio.com/product-category/stickers-spreaders/",
//   },
//   {
//     imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/organeem-10000-scaled-300x300.jpg",
//     alt: "Neem Based Products",
//     title: "Neem Based Products",
//     href: "https://kaybeebio.com/product-category/neem-based-products/",
//   },
//   {
//     imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/06/Novazyme-1-300x300.png",
//     alt: "Granule Based Products",
//     title: "Granule Based Products",
//     href: "https://kaybeebio.com/product-category/granule-based-products/",
//   },
//   {
//     imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/Nova-Zyme-scaled-300x300.jpg",
//     alt: "Plant Growth Regulators",
//     title: "Plant Growth Regulators",
//     href: "https://kaybeebio.com/product-category/plant-growth-regulator/",
//   },
// ];

// const Services = () => {
//   const [isInView, setIsInView] = useState([]);

//   useEffect(() => {
//     const observer = new IntersectionObserver(
//       (entries) => {
//         entries.forEach((entry, index) => {
//           if (entry.isIntersecting) {
//             entry.target.classList.add("zoom-in-effect");
//           } else {
//             entry.target.classList.remove("zoom-in-effect");
//           }
//         });
//       },
//       { threshold: 0.5 } // Trigger when 50% of the element is in view
//     );

//     const topCatsItems = document.querySelectorAll(".top-cats-list-item");

//     topCatsItems.forEach((item) => {
//       observer.observe(item);
//     });

//     // Cleanup observer on component unmount
//     return () => {
//       observer.disconnect();
//     };
//   }, []);

//   return (
//     <section className="top-cats-sec py-5">
//       <div className="container">
//         <h3 className="section-heading text-center mb-5">Top Categories</h3>
//         <div className="row d-flex justify-content-between g-4">
//           {categories.map((category, index) => (
//             <div
//               className="col-md-2 col-sm-4 mb-4 top-cats-list-item"
//               key={index}
//             >
//               <div className="card">
//                 <a href={category.href} className="text-danger">
//                   <img
//                     src={category.imgSrc}
//                     alt={category.alt}
//                     className="card-img-top img-fluid"
//                   />
//                   <div className="card-body text-center">
//                     <h5 className="card-title">{category.title}</h5>
//                   </div>
//                 </a>
//               </div>
//             </div>
//           ))}
//         </div>
//       </div>
//     </section>
//   );
// };

// export default Services;
import React, { useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "../Css/style.css";

const categories = [
  {
    imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/downy-raze-4-300x300.jpg",
    alt: "Bio Pesticides Products",
    title: "Bio Pesticides Products",
    href: "https://kaybeebio.com/product-category/bio-pesticides/",
  },
  {
    imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/balanstick-1-300x300.jpg",
    alt: "Stickers & Spreaders",
    title: "Stickers & Spreaders",
    href: "https://kaybeebio.com/product-category/stickers-spreaders/",
  },
  {
    imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/organeem-10000-scaled-300x300.jpg",
    alt: "Neem Based Products",
    title: "Neem Based Products",
    href: "https://kaybeebio.com/product-category/neem-based-products/",
  },
  {
    imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/06/Novazyme-1-300x300.png",
    alt: "Granule Based Products",
    title: "Granule Based Products",
    href: "https://kaybeebio.com/product-category/granule-based-products/",
  },
  {
    imgSrc: "https://kaybeebio.com/wp-content/uploads/2023/05/Nova-Zyme-scaled-300x300.jpg",
    alt: "Plant Growth Regulators",
    title: "Plant Growth Regulators",
    href: "https://kaybeebio.com/product-category/plant-growth-regulator/",
  },
];

const Services = () => {
  useEffect(() => {
    const observer = new IntersectionObserver(
      (entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            entry.target.classList.add("zoom-in-effect");
          } else {
            entry.target.classList.remove("zoom-in-effect");
          }
        });
      },
      { threshold: 0.5 }
    );

    const topCatsItems = document.querySelectorAll(".top-cats-list-item");

    topCatsItems.forEach((item) => {
      observer.observe(item);
    });

    return () => {
      observer.disconnect();
    };
  }, []);

  return (
    <section className="top-cats-sec py-5 bg-light">
      {/* Container updated to fluid */}
      <div className="container-fluid">
        <h3 className="section-heading text-center mb-5">Top Categories</h3>
        <div className="row g-4 justify-content-center">
          {categories.map((category, index) => (
            <div
              className="col-lg-2 col-md-4 col-12 top-cats-list-item"
              key={index}
            >
              <div className="card h-100 border-0 shadow-sm">
                <a href={category.href} className="text-decoration-none text-dark">
                  <img
                    src={category.imgSrc}
                    alt={category.alt}
                    className="card-img-top img-fluid"
                  />
                  <div className="card-body text-center">
                    <h5 className="card-title mb-0">{category.title}</h5>
                  </div>
                </a>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Services;
